# Proposta Debian Day 2024 - Porto Alegre

## O que é o Debian?

O Debian[^1] é um sistema operacional livre, baseado no kernel Linux. O projeto teve início em 1993, hoje é uma das distribuições mais antigas com desenvolvimento ativo e base para muitas outras distribuições como Ubuntu, Kali Linux e Proxmox.

Todo o desenvolvimento é aberto e realizado por voluntários. Possui uma organização consistente através do contrato social do Debian[^2] e a definição Debian de Software Livre (DFSG - Debian Free Software Guidelines) como parte deste contrato, que descrevem o compromisso do Debian com o Software Livre e com a comunidade de Software Livre.

Os números do Debian:

 - 1019 membros oficiais[^3]
 - 1592 contribuidores e 20 times nos últimos 12 meses[^4]
 - 24 Debian Developers no Brasil, ocupando a 10ª colocação mundial[^5]
 - 9 arquiteturas oficiais[^6]
 - 64.419 pacotes de softwares na última release[^7]

## O que é o Debian Day?

O Projeto Debian foi fundado oficialmente por Ian Murdock em 16/08/1993. A comunidade Debian celebra seu aniversário, o Debian Day (Dia do Debian)[^8], nesta data todo ano ou no primeiro sábado posterior à data.
Diversas comunidades regionais costumam organizar encontros para celebrar a data. Não há um formato restrito para as atividades, o foco é celebrar o aniversário do projeto, geralmente ocorrem festivais de instalação, assinatura de chaves e palestras.

A comunidade brasileira possui tradição na realização do Debian Day[^9], atividade que é amplamente apoiada pela comunidade nacional do projeto através do Debian Brasil[^10].

## O que é o Debian RS?

O Debian RS[^11] é um GUD (grupo de usuários(as) do Debian). No Brasil, somos 6 grupos ativos. O Debian RS foi criado em 2001 durante o 2º Fórum Internacional Software Livre realizado na UFRGS. O grupo possui longa trajetória, sendo responsável pela realização de muitas atividades da comunidade Debian no estado. Passou por um período de menor mobilização nos últimos anos, neste ano buscamos retomar o contato e atividades do grupo.

O Debian Day foi realizado em Porto Alegre de 2004 a 2012 (pelo que temos registro).

## Objetivos do Debian Day 2024 em Porto Alegre

As expectativas para o Debian Day 2024 em Porto Alegre é realizá-lo em 17/08/2024 (sábado) para celebrar o aniversário do projeto, reconectar a comunidade e aproximar pessoas que queiram contribuir[^12] com o projeto.

Desde já agradeço a atenção e interesse sobre o evento.
Me coloco à disposição, seguem minhas formas de contato:

```
Preencher com formas de contato.
```

## Referências externas

[^1]: https://www.debian.org/
[^2]: https://www.debian.org/social_contract
[^3]: https://nm.debian.org/members/
[^4]: https://contributors.debian.org/
[^5]: https://people.debian.org/~eriberto/udd/dd-by-country.html
[^6]: https://www.debian.org/ports/
[^7]: https://www.debian.org/News/2023/20230610
[^8]: https://wiki.debian.org/pt_BR/DebianDay
[^9]: https://wiki.debian.org/pt_BR/DebianDay#Estat.2BAO0-sticas
[^10]: https://debianbrasil.org.br/
[^11]: https://wiki.debian.org/Brasil/GUD/RS
[^12]: https://www.debian.org/intro/help