# Como contribuir com este repositório

Dúvidas e sugestões podem ser tratadas via issue e alterações via merge request.

Este repositório estará em constante mudança, conforme as necessidades da comunidade.
